# IKA: Independent Kernel Approximator

This repository contains the code for reproducing the results
shown in the paper [IKA: Independent Kernel Approximator](https://arxiv.org/abs/1809.01353).

## Installation
This code was written for Python 3. All requirement can be installed with:
```
pip install -r requirements.txt
```

## Execution
The file `sample_patches.py` contains the code that 
samples 1'000'000 patches from the STL-10 dataset and
does all the preprocessing. It has two optional arguments
```
-d, --download        Flag that specifies if it is needed to download SLT-10 data
-p PATH, --path PATH  path of the STL-10 'unlabeled_X.bin' file
```


The file `compare.py` contains the code that does the comparison on
the patches sampled by `sample_patches.py`. It runs multiple experiments in parallel
and takes a positional argument that is the number of processes that will be used 
to run the experiments. This scripts append new results to `results.json`.


All the results presented in the paper where obtained by running the script `reproduce.sh`.

## Results
In the paper aggregated results are presented, raw results are stored in the file `results.json`.
<br>**Note** Because the experiments were run in parallel the field "time" is very inaccurate.


