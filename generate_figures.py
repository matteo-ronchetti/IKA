import json
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys


def grouped_error_plot(df, xlabel, ylabel, name=None, color="blue"):
    grouped = df.groupby([xlabel])
    err = grouped[ylabel].mean()
    std = grouped[ylabel].std()

    plt.errorbar(grouped[xlabel].mean(), err, yerr=std, fmt="o-", capsize=3, label=name, ecolor="gray", elinewidth=1,
                 color=color, markersize=3)
    plt.xticks(df[xlabel].unique())


def select(df, **kwargs):
    res = df
    for k in kwargs:
        if k in res:
            res = res[res[k] == kwargs[k]]
        else:
            res = res[res[k.replace("_", " ")] == kwargs[k]]

    return res


# colors = ["#01668D", "#F77401"]
colors = ["#2980B9", "#f07833"]

with open("results.json") as f:
    df = pd.DataFrame(json.load(f))

vdf = df[df["num filters"] == df["out dim"]]

# IKA vs Nystrom with random filters
plt.figure()
ika = select(vdf, method="IKA", filter_type="random", sample_size=15000)
nystrom = select(vdf, method="Nystrom", filter_type="random")
print("Mean improvement on random filters: %f" % float(
    (1 - ika.groupby(["num filters"])["abs_err"].mean() / nystrom.groupby(["num filters"])["abs_err"].mean()).mean()))
grouped_error_plot(ika, 'num filters', "abs_err",
                   name="IKA", color=colors[0])
grouped_error_plot(nystrom, 'num filters', "abs_err", name="Nystrom",
                   color=colors[1])
plt.xlabel("Number of Filters")
plt.ylabel("Absolute Error")
plt.legend()
plt.savefig("figures/vs_random_abs_err.png", dpi=600)

plt.figure()
grouped_error_plot(select(vdf, method="IKA", filter_type="random", sample_size=15000), 'num filters', "rel_err",
                   name="IKA", color=colors[0])
grouped_error_plot(select(vdf, method="Nystrom", filter_type="random"), 'num filters', "rel_err", name="Nystrom",
                   color=colors[1])
plt.xlabel("Number of Filters")
plt.ylabel("Relative Error")
plt.legend()
plt.savefig("figures/vs_random_rel_err.png", dpi=600)

# IKA vs Nystrom with kmeans filters
plt.figure()
ika = select(vdf, method="IKA", filter_type="kmeans", sample_size=15000)
nystrom = select(vdf, method="Nystrom", filter_type="kmeans")
print("Mean improvement on kmeans filters: %f" % float(
    (1 - ika.groupby(["num filters"])["abs_err"].mean() / nystrom.groupby(["num filters"])["abs_err"].mean()).mean()))
grouped_error_plot(ika, 'num filters', "abs_err",
                   name="IKA", color=colors[0])
grouped_error_plot(nystrom, 'num filters', "abs_err", name="Nystrom",
                   color=colors[1])

plt.xlabel("Number of Filters")
plt.ylabel("Absolute Error")
plt.legend()
plt.savefig("figures/vs_kmeans_abs_err.png", dpi=600)

plt.figure()
grouped_error_plot(select(vdf, method="IKA", filter_type="kmeans", sample_size=15000), 'num filters', "rel_err",
                   name="IKA", color=colors[0])
grouped_error_plot(select(vdf, method="Nystrom", filter_type="kmeans"), 'num filters', "rel_err", name="Nystrom",
                   color=colors[1])
plt.xlabel("Number of Filters")
plt.ylabel("Relative Error")
plt.legend()
plt.savefig("figures/vs_kmeans_rel_err.png", dpi=600)

# IKA (random) sample size vs error 
plt.figure()
grouped_error_plot(select(vdf, method="IKA", num_filters=128, filter_type="random", eps=1e-12), 'sample size',
                   "abs_err",
                   name="IKA n=128", color=colors[0])
plt.xlabel("Sample Size")
plt.ylabel("Absolute Error")
plt.savefig("figures/ika_random_samplesize.png", dpi=600)

# IKA vs Nystrom with reduced out dim
plt.figure()
grouped_error_plot(select(df, method="IKA", num_filters=128, filter_type="random", eps=1e-12, sample_size=15000),
                   'out dim', "abs_err",
                   name="IKA", color=colors[0])
grouped_error_plot(select(df, method="Nystrom", num_filters=128, filter_type="random", eps=1e-12), 'out dim', "abs_err",
                   name="Nystrom", color=colors[1])
plt.xlabel("Output Dimension")
plt.ylabel("Absolute Error")
plt.legend()
plt.savefig("figures/vs_out_dim.png", dpi=600)

plt.show()
