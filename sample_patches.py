import numpy as np
import os
import sys
import tarfile
import argparse
from urllib.request import urlretrieve


def stl_reader(path, batch_size):
    with open(path, "rb") as f:
        while True:
            imgs = np.fromfile(f, dtype=np.uint8, count=batch_size * 3 * 96 * 96).reshape(-1, 3, 96, 96)
            if imgs.shape[0] == 0:
                break
            yield imgs


def global_contrast_normalization(X, eps):
    initial_shape = X.shape
    X = X.reshape(X.shape[0], -1)
    X -= np.mean(X, axis=1)[:, None]
    X /= np.sqrt(eps + np.mean(X ** 2, axis=1))[:, None]

    return X.reshape(initial_shape)


def download_extract_stl():
    dest_directory = "stl-10"
    if not os.path.exists(dest_directory):
        os.makedirs(dest_directory)
    filename = "stl10_binary.tar.gz"
    filepath = os.path.join(dest_directory, filename)
    if not os.path.exists(filepath):
        def _progress(count, block_size, total_size):
            sys.stdout.write('\rDownloading %s %.2f%%' % (filename,
                                                          float(count * block_size) / float(total_size) * 100.0))
            sys.stdout.flush()

        filepath, _ = urlretrieve('http://ai.stanford.edu/~acoates/stl10/stl10_binary.tar.gz', filepath,
                                  reporthook=_progress)
        print('Downloaded', filename)
        tarfile.open(filepath, 'r:gz').extractall(dest_directory)


# constants
n_batches = 100
n_patches = 1000000
size = 7
gcn_eps = 10
normalization_eps = 1e-5

n_ppb = n_patches // n_batches
patches = np.empty((n_patches, 3 * size * size), dtype=np.float32)
total_patches = 0

parser = argparse.ArgumentParser(description='Extract and preprocess patches')
parser.add_argument('-d', "--download", action='store_true', default=False,
                    help="Flag that specifies if it is needed to download SLT-10 data")
parser.add_argument('-p', "--path", default="stl-10/unlabeled_X.bin", help="path of the STL-10 'unlabeled_X.bin' file")
args = parser.parse_args()

if args.download:
    download_extract_stl()
print("Sampling patches...")
for X in stl_reader(args.path, 1000):
    X = global_contrast_normalization(X.astype(np.float32), gcn_eps)

    pi = np.random.randint(0, X.shape[0], n_ppb)
    px = np.random.randint(0, X.shape[3] - size, n_ppb)
    py = np.random.randint(0, X.shape[2] - size, n_ppb)

    for i in range(n_ppb):
        patches[total_patches + i] = X[pi[i], :, py[i]:py[i] + size, px[i]:px[i] + size].reshape(-1)

    total_patches += n_ppb

patches = patches[:total_patches]
print("Sampled %d patches" % total_patches)

print("Whitening patches...")
patches -= np.mean(patches, axis=0)[None, :]
cov = np.dot(patches.T, patches) / patches.shape[0]
vals, vecs = np.linalg.eigh(cov)
W = np.dot(vecs, np.dot(np.diag(1.0 / np.sqrt(vals + 1e-6)), vecs.T))
patches = np.dot(patches, W)

print("Normalizing patches...")
patches /= np.maximum(np.linalg.norm(patches, axis=1), normalization_eps)[:, None]

np.save("patches.npy", patches)
