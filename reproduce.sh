#!/usr/bin/env bash
set -e

for i in {1..15}
do
    echo "Executing run number $i"
    python sample_patches.py
    python compare.py 9
done
