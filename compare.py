import numpy as np
import scipy.linalg
import time
import json
import os
import sys
from multiprocessing import Pool, cpu_count
from sklearn.cluster import KMeans, MiniBatchKMeans


def kernel(sigma):
    return lambda x: np.exp((x - 1) / sigma)


def estimate_sigma(patches, k=10, n=2048):
    A = patches[np.random.permutation(patches.shape[0])[:n]]
    B = patches[np.random.permutation(patches.shape[0])[:n]]

    distances = 1 - np.dot(A, B.T)
    # print("Distances", np.mean(distances), np.min(distances), np.max(distances))
    sigma = np.percentile(distances, k)
    return float(sigma)


class Nystrom:
    def __init__(self, Z, k, eps=0):
        self.Z = Z
        self.k = k
        self.K = k(np.dot(Z, Z.T))
        self.l, self.v = np.linalg.eigh(self.K + eps * np.eye(self.K.shape[0]))
        self.l = self.l.clip(1e-12, 1e30)

        self.psi = None

    def compute(self, r):
        self.psi = np.dot(np.diag((self.l[r:] ** (-1 / 2))), self.v[:, r:].T)

    def phi(self, X_):
        return np.dot(self.k(np.dot(X_, self.Z.T)), self.psi.T)

    def test(self, X, n=5000, niter=10):
        abs_err = 0
        rel_err = 0

        for i in range(niter):
            S = X[np.random.permutation(X.shape[0])[:n]]
            V = X[np.random.permutation(X.shape[0])[:n]]

            rk = self.k(np.dot(S, V.T))
            ak = np.dot(self.phi(S), self.phi(V).T)

            abs_err += np.mean(np.abs(rk - ak))
            rel_err += np.mean(np.abs(rk - ak) / rk)

        return abs_err / niter, rel_err / niter


class IKA:
    def __init__(self, f, k):
        self.f = f
        self.k = k

        self.P = None
        self.M = None

        self.f = f
        self.psi = None

    def accumulate_P_M(self, X, ss=2000):
        S = X[np.random.permutation(X.shape[0])[:ss]]

        B = self.f(S) / np.sqrt(ss)
        self.P = np.dot(B.T, B)

        G = self.k(np.dot(S, S.T))
        self.M = np.dot(B.T, np.dot(G, B)) / ss

    def compute(self, eps=1e-12, r=0):
        self.l, self.v = scipy.linalg.eigh(self.M, self.P + eps * np.eye(self.P.shape[0]))
        self.l = self.l.clip(0, 1e30)
        self.psi = np.dot(np.diag(self.l[r:]) ** (1 / 2), self.v[:, r:].T)

    def test(self, X, n=5000, niter=10):
        abs_err = 0
        rel_err = 0

        def phi(X):
            return np.dot(self.f(X), self.psi.T)

        for i in range(niter):
            S = X[np.random.permutation(X.shape[0])[:n]]
            V = X[np.random.permutation(X.shape[0])[:n]]

            rk = self.k(np.dot(S, V.T))
            ak = np.dot(phi(S), phi(V).T)

            abs_err += np.mean(np.abs(rk - ak))
            rel_err += np.mean(np.abs(rk - ak) / rk)

        return abs_err / niter, rel_err / niter


def run_comparison(cfg):
    k = kernel(cfg["sigma"])
    patches = cfg["patches"]
    num_filters = cfg["filters"].shape[0]

    if cfg["method"] == "nystrom":
        s = time.time()
        app = Nystrom(cfg["filters"], k, eps=cfg["eps"])
        app.compute(r=num_filters - cfg["out_dim"])
        t = time.time() - s
        abs_err, rel_err = app.test(patches)
        res = {"method": "Nystrom", "abs_err": abs_err, "rel_err": rel_err, "eps": cfg["eps"], "time": t,
               "sigma": cfg["sigma"],
               "sigma percentile": 10, "num filters": num_filters, "out dim": cfg["out_dim"]}
    else:
        s = time.time()
        app = IKA(lambda x: k(np.dot(x, cfg["filters"].T)), k)
        app.accumulate_P_M(patches[:800000], ss=cfg["sample size"])
        app.compute(eps=cfg["eps"], r=num_filters - cfg["out_dim"])
        t = time.time() - s
        abs_err, rel_err = app.test(patches[800000:])
        res = {"method": "IKA", "abs_err": abs_err, "rel_err": rel_err, "eps": cfg["eps"], "time": t,
               "sample size": cfg["sample size"], "sigma": cfg["sigma"], "sigma percentile": 10,
               "num filters": num_filters, "out dim": cfg["out_dim"]}

    res["filter_type"] = cfg["filter_type"]
    print(res)
    return res


def main(n_workers):
    print("Using %d workers" % n_workers)
    # n_filters = int(sys.argv[1]) if len(sys.argv) >= 2 else 64
    sigma_perc = 10
    res_path = "results.json"

    if os.path.exists(res_path):
        with open(res_path) as f:
            res = json.load(f)
    else:
        res = []

    print("Loading data...")
    patches = np.load("patches.npy")

    sigma = estimate_sigma(patches, sigma_perc)
    print("Chosen Sigma:", sigma)

    cfgs = []
    eps = 1e-12

    for n_filters in range(32, 129, 16):
        filters = patches[np.random.permutation(patches.shape[0])[:n_filters]]

        # test Nystrom method
        cfgs.append({"method": "nystrom", "patches": patches, "filters": filters, "filter_type": "random", "eps": eps,
                     "sigma": sigma, "out_dim": n_filters})

        # test IKA with different sample sizes
        for ss in [1000, 5000, 10000, 15000]:
            cfgs.append({"method": "IKA", "patches": patches, "filters": filters, "filter_type": "random", "eps": eps,
                         "sigma": sigma, "sample size": ss, "out_dim": n_filters})

        kmeans = MiniBatchKMeans(n_clusters=n_filters, n_init=3, max_iter=50, batch_size=n_filters * 100).fit(patches)
        filters = kmeans.cluster_centers_.copy()
        filters /= np.linalg.norm(filters, axis=1)[:, None]

        # test Nystrom method with kmeans filters
        cfgs.append({"method": "nystrom", "patches": patches, "filters": filters, "filter_type": "kmeans", "eps": eps,
                     "sigma": sigma, "out_dim": n_filters})

        for ss in [1000, 5000, 10000, 15000]:
            cfgs.append({"method": "IKA", "patches": patches, "filters": filters, "filter_type": "kmeans", "eps": eps,
                         "sigma": sigma, "sample size": ss, "out_dim": n_filters})

    n_filters = 128
    for n_out_dim in range(32, 129, 16):
        filters = patches[np.random.permutation(patches.shape[0])[:n_filters]]

        # test Nystrom method
        cfgs.append({"method": "nystrom", "patches": patches, "filters": filters, "filter_type": "random", "eps": eps,
                     "sigma": sigma, "out_dim": n_out_dim})

        cfgs.append({"method": "IKA", "patches": patches, "filters": filters, "filter_type": "random", "eps": eps,
                     "sigma": sigma, "sample size": 15000, "out_dim": n_out_dim})

    print("Need to run %d experiments" % len(cfgs))

    pool = Pool(n_workers)
    res += pool.map(run_comparison, cfgs)

    with open(res_path, "w") as f:
        json.dump(res, f)


if __name__ == "__main__":
    if len(sys.argv) >= 2:
        n_workers = int(sys.argv[1])
    else:
        n_workers = cpu_count()

    main(n_workers)
